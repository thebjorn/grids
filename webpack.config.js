const path = require('path')
// const CssoWebpackPlugin = require('csso-webpack-plugin').default;
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const potcssColorMod = require('postcss-color-mod-function');

module.exports = {
    mode: 'development',
    entry: './styles/main.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist')
    },
    devServer: {
        static: path.resolve(__dirname, 'dist'),
        port: 8080,
        hot: true
    },
    module: {
        rules: [
            {
                test: /\.(scss)$/,
                use: [
                    // prod - extract css to file
                    // {
                    //     loader: MiniCssExtractPlugin.loader,
                    // },
                    // // dev - load css through js / hotload
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                ident: 'postcss',
                                plugins: () => [
                                    require('autoprefixer')
                                ]
                            }
                        }
                    },
                    {
                        loader: 'sass-loader',
                        // options: {
                        //     sassOptions: {
                        //         loadPath: [
                        //             './styles',
                        //         ]
                        //     }
                        // }
                    }
                ]
            }, {
                test: /\.(less)$/,
                use: [
                    // prod - extract css to file
                    // {
                    //     loader: MiniCssExtractPlugin.loader,
                    // },
                    // // dev - load css through js / hotload
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                ident: 'postcss',
                                plugins: () => [
                                    require('autoprefixer')
                                ]
                            }
                        }
                    },
                    {
                        loader: 'less-loader',
                    }
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css",
        }),
    //     // new CssoWebpackPlugin(),
    ]
}
